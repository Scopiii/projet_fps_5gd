﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grappin : MonoBehaviour {

	public GameObject player;
	CharacterController controller;
	bl_PlayerMovement playerMovement;

	Vector3 targetPos;

	float grappinSpeed;
	bool keyGot;

	GameObject gameManager;
	bl_RoomMenu roomMenu;

	[Header("Grappin setup")]
	[Range(10, 200)]
	public float minimumGrappinRange;

	[Range(0, 100)]
	public float minimumDropDistance = 8f;

	[Header("Grappin jauge")]

	float cooldownTime;

	[Range(1, 20)]
	public float totalTime; // Total time the player can use the grappin in seconds;

	[Range(1, 20)]
	public float reloadTime; // The time the jauge reloads 

	[Range(1, 20)]
	public float diminutionTime;


	bool canGrab;

	bool isUsingGrappin;

	Image rewindUI;

	Animator wheelAnim;
	Image wheelSprite;

	bool wheelCharged;



	// Use this for initialization
	void Start () {

		GameObject ui = GameObject.Find ("CooldownPower");
		rewindUI = ui.GetComponent<Image> ();

		wheelAnim = ui.GetComponent<Animator> ();
		wheelSprite = ui.GetComponent<Image> ();

		controller = player.GetComponent<CharacterController>();
		playerMovement = player.GetComponent<bl_PlayerMovement> ();

		gameManager = GameObject.Find ("_GameManager");
		roomMenu = gameManager.GetComponent<bl_RoomMenu> ();

		if (roomMenu.selectedClass != bl_RoomMenu.EnumerationClass.Grappin) {
			this.enabled = false;
		}
			
	}

	// Update is called once per frame
	void Update () {
		
		UpdateCoolDown ();

		if (Input.GetKeyDown (KeyCode.E) && canGrab && cooldownTime >= totalTime) {
			DefineGrappinTarget ();
			keyGot = true;
			isUsingGrappin = true;
			StopCoroutine ("SimulateReleaseForce");

			wheelAnim.SetTrigger ("PowerUsed");
			wheelCharged = false;
		}

		if (Input.GetKey (KeyCode.E))
			FlyToPosition (targetPos);

		if (Input.GetKeyUp (KeyCode.E)) {
			UnlauchGrappin ();
			isUsingGrappin = false;
		}

		CheckGrappinAvailability ();

	}

	void UpdateCoolDown () {

		if (!isUsingGrappin) {
			if (cooldownTime < totalTime) {
				cooldownTime += Time.deltaTime * reloadTime;
				wheelSprite.color = Color.white;
			}
		} else {
			cooldownTime -= Time.deltaTime;
			wheelSprite.color = Color.white;
		}

		if (cooldownTime >= totalTime && !wheelCharged) {

			wheelAnim.SetTrigger ("WheelCharged");
			wheelCharged = true;
			wheelSprite.color = Color.yellow;
		}

		rewindUI.fillAmount = cooldownTime / totalTime;

//		Debug.Log ("Grappin cooldown  " + cooldownTime + " ratio  " + cooldownTime/totalTime);

	}

	void DefineGrappinTarget () {

		RaycastHit hit;

		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
		{
			Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
//			Debug.Log("Did Hit");
//			Debug.Log ("Hited object position  " + hit.point);
			targetPos = hit.point;
		}

	}

	void UnlauchGrappin () {

		controller.enabled = true;
		playerMovement.canSecond = true;
		//grappinSpeed = 0.3f;
		StartCoroutine ("SimulateReleaseForce", targetPos);
	}

	void CheckGrappinAvailability () {

		RaycastHit hit;

		if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out hit, minimumGrappinRange)) {

			canGrab = true;

		} else {
			canGrab = false;
		}

	}


	void FlyToPosition (Vector3 hitPosition) {


		if (Vector3.Distance (hitPosition, player.transform.position) > minimumDropDistance && keyGot) {

			playerMovement.grounded = false;

			grappinSpeed += (Time.deltaTime / 1f);

			player.transform.position = Vector3.MoveTowards (player.transform.position, hitPosition, grappinSpeed);
			controller.enabled = false;
		} else {

			controller.enabled = true;
			keyGot = false;
		}

	}

	void CheckCollision (Vector3 hitPosition) {

		if (Vector3.Distance (player.transform.position, hitPosition) <= 0.1f) {
			//gotHit = true;
			Debug.Log ("Player got hit in grappin");
			UnlauchGrappin ();
		}


	}

	IEnumerator SimulateReleaseForce (Vector3 positionToGo) {

		while (grappinSpeed >= 0) {
			yield return new WaitForEndOfFrame ();
			grappinSpeed -= (Time.deltaTime);
			player.transform.position = Vector3.MoveTowards (player.transform.position, positionToGo, grappinSpeed);

			if (playerMovement.grounded)
				grappinSpeed = 0;

		}


	}



}
