﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXSoundManager : MonoBehaviour {

	public AudioClip[] fxClips;

	AudioSource source;

	// Use this for initialization
	void Start () {

		source = Camera.main.gameObject.GetComponent<AudioSource> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void playFXSound (int index) {

		source.PlayOneShot (fxClips [index]);

	}
}
