﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;
using UnityEngine.UI;

public class TimeRewind : MonoBehaviour {

	Clock clock;
	LocalClock mouseClock;

	public int playerIndex;

	public GameObject mouse;
	public GameObject player;

	bl_MouseLook mouseLook;
	bl_MouseLook camMouseLook;

	public Quaternion yBaseRotation;

	public List<float> mouseYPos = new List<float>();


	bool shouldStockYPos = true;

	CharacterController charController;
	public static PlayerClass pclass;

	GameObject gameManager;
	bl_RoomMenu roomMenu;

	[Header("Rewind Setup")]

	[Range(0.1f, 9f)]
	public float rewindSpeed;

	[Header("Rewind Jauge")]


	float cooldownTime;

	[Range(1, 20)]
	public float totalTime;

	[Range(1, 20)]
	public float reloadTime;

	[Range(1, 20)]
	public float diminutionTime;

	bool isUsingRewind;

	Image rewindUI;

	bool wheelCharged;

	Animator wheelAnim;
	Image wheelSprite;

	// Use this for initialization
	void Start () {

		GameObject ui = GameObject.Find ("CooldownPower");
		rewindUI = ui.GetComponent<Image> ();

		wheelAnim = ui.GetComponent<Animator> ();
		wheelSprite = ui.GetComponent<Image> ();

		mouseLook = GetComponent<bl_MouseLook> ();

		mouse = transform.Find ("Heat").transform.Find ("Mouse").gameObject;
		mouseClock = mouse.GetComponent<LocalClock> ();
		camMouseLook = mouse.GetComponent<bl_MouseLook> ();

		charController = GetComponent<CharacterController> ();

		gameManager = GameObject.Find ("_GameManager");
		roomMenu = gameManager.GetComponent<bl_RoomMenu> ();


		if (roomMenu.selectedClass != bl_RoomMenu.EnumerationClass.Rewind) {
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		UpdateCoolDown();

		if (playerIndex == 1) {
			clock = Timekeeper.instance.Clock ("Player 1");
		} else if (playerIndex == 2) {
			clock = Timekeeper.instance.Clock ("Player 2");
		}

		if (roomMenu.selectedClass == bl_RoomMenu.EnumerationClass.Rewind) {
			
			if (Input.GetKey (KeyCode.Y) && cooldownTime >= totalTime) {
				isUsingRewind = true;
				Rewind ();
				wheelAnim.SetTrigger ("PowerUsed");
				wheelCharged = false;
		

			}

			if (Input.GetKeyUp (KeyCode.Y)) {
				UnRewind ();
				isUsingRewind = false;
			}

		}
		//if (shouldStockYPos)
		//StoreMouseYPos ();

	}


	void UpdateCoolDown () {

		if (!isUsingRewind) {
			if (cooldownTime <= totalTime) {
				cooldownTime += Time.deltaTime;
				wheelSprite.color = Color.white;
			}

		} else {
			cooldownTime -= Time.deltaTime * diminutionTime;
			wheelSprite.color = Color.white;
		}

		if (cooldownTime >= totalTime && !wheelCharged) {

			wheelAnim.SetTrigger ("WheelCharged");
			wheelCharged = true;
			wheelSprite.color = Color.yellow;
		}

		rewindUI.fillAmount = cooldownTime / totalTime;


		Debug.Log ("Grappin cooldown  " + cooldownTime + " ratio  " + cooldownTime/totalTime);

	}

	void StoreMouseYPos () {

		float yMouseRot = camMouseLook.rotationY;
		mouseYPos.Add (yMouseRot); 

	}

	void RewindMouseYPos () {
 

		for (int i = mouseYPos.Capacity - 1; i > 200; i -= 1) {

			Debug.Log (mouseYPos.Capacity + "  capacty  " + i);
			camMouseLook.rotationY = mouseYPos [i];
		}	 


	}

	void Rewind () {
 
			clock.localTimeScale = -rewindSpeed; 

			shouldStockYPos = false;
			mouse.transform.Rotate (Vector3.zero);
			//RewindMouseYPos ();
			//mouseClock.localTimeScale = -2f;
			camMouseLook.enabled = false;

			float previousRotY = camMouseLook.rotationY;
			mouseYPos.Add (previousRotY);
			charController.enabled = false;
			mouseLook.rotationX = 0;
 
	}

	void StockPreviousRotation () {



	}

	void UnRewind ()
	{
		
		clock.localTimeScale = 1;
		shouldStockYPos = true;
		camMouseLook.enabled = true;
		charController.enabled = true;
		bl_MouseLook mLook = GetComponent<bl_MouseLook> ();
		mLook.rotationX = 0;

		/*	
		bl_MouseLook mouseToDestroy = GetComponent<bl_MouseLook> ();
		Destroy (mouseToDestroy);


		mouse = transform.Find ("Heat").transform.Find ("Mouse").gameObject;
		bl_MouseLook mouseToDestroyMouse = mouse.GetComponent<bl_MouseLook> ();
		Destroy (mouseToDestroy);

		bl_MouseLook newMouseLookPlayer = gameObject.AddComponent (typeof(bl_MouseLook)) as bl_MouseLook;

		bl_MouseLook newMouseLookMouse = mouse.AddComponent (typeof(bl_MouseLook)) as bl_MouseLook;
		camMouseLook = newMouseLookMouse;

		//bl_MouseLook newMouse = GetComponent<bl_MouseLook> ();
*/



 
	}
}