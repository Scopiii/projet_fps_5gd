﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {



	GameObject gameManager;
	bl_RoomMenu roomMenu;


	GameObject dashLocalizator; // The game object we gonna use to show the player where he is going to dash

	[SerializeField]
	GameObject currentDash;

	[SerializeField]
	[Range(0, 50)]
	float dashRange= 10f;

	float yOffset = 9.8f;
	float zOffset = 4f;

	GameObject parent; 

	bool hitObstacle;

	public GameObject sphere; // The game object used to show the trajectory
	public List<GameObject> spheres = new List<GameObject>(); // The list that will contain all the spheres
 


	// Use this for initialization
	void Start () {



		gameManager = GameObject.Find ("_GameManager");
		roomMenu = gameManager.GetComponent<bl_RoomMenu> ();

		if (roomMenu.selectedClass != bl_RoomMenu.EnumerationClass.Dash) {
			this.enabled = false;
		} else {
			Debug.Log ("Selected class is Dash");
		}
 
		LoadDashAsset (); 

		
	}

	void LoadDashAsset () {

		dashLocalizator = Resources.Load("Dash/DashLocationVisualizator") as GameObject;

		GameObject newDash =  Instantiate (dashLocalizator) as GameObject;
		newDash.name = "DashVisualizer";

		newDash.transform.parent = this.transform;
			
		currentDash = newDash;
		currentDash.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z + dashRange);

		parent = transform.parent.gameObject.transform.parent.gameObject;
		Debug.Log (" Parent name  " + parent.name);
	}

	 
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.E)) {
			ShowDashLocation (transform.TransformDirection (Vector3.forward));
			//DrawLine (currentDash.transform.position);*
			CheckObstacles(); 

		}
		if (Input.GetKeyUp (KeyCode.E)) {
			UnshowDashLocalition ();
			if (!hitObstacle)
			Teleport ();
 
			//DisableLine ();
		}

//		Debug.Log ("You are looking in this position   " + transform.TransformDirection (Vector3.forward));
		Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward));


	}

	void ShowDashLocation (Vector3 playerForward) {


		currentDash.SetActive (true);


	}

	void CheckObstacles () {

		RaycastHit hit;

		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, dashRange + 1f))
		{
			Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * dashRange, Color.yellow);

			Debug.Log ("Hitted  " + hit.transform.gameObject.name);

			if (hit.transform.gameObject.name == "DashVisualizer") {

				// Good Feedback
				hitObstacle = false;

			} else {
				hitObstacle = true;
			}
				

		}

	}

	void UnshowDashLocalition () {

		currentDash.SetActive (false);

	}
 

	void Teleport () {

		GameObject visualizer = transform.Find ("DashVisualizer").gameObject;

		Debug.Log("You should teleport here  " + visualizer.transform.position);
		parent.transform.position = visualizer.transform.position;

	}

  
 

}
